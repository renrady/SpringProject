package com.example.demo.controller;

import com.example.demo.model.Category;
import com.example.demo.service.category.CategoryService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @GetMapping("/category")
    public String category(Model model){
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("formAdd", true);
        model.addAttribute("category", new Category());
        return "category";
    }

//    @GetMapping("/addCategory")
//    public String insert(Model model){
//        model.addAttribute("category", new Category());
//        return "category";
//    }
//
    @PostMapping("/category")
    public String saveInsert(@ModelAttribute @Valid Category category, BindingResult result, Model model){
        if (result.hasErrors()){
            model.addAttribute("formAdd", true);
            return "redirect:/category";
        }
        categoryService.insert(category);
        return "redirect:/category";
    }

    @GetMapping("/category/update/{id}")
    public String update(@PathVariable("id") int id, Model model){
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("formAdd", false);
        model.addAttribute("category", categoryService.findOne(id));
        return "category";
    }

    @PostMapping("/category/update")
    public String saveUpdate(@ModelAttribute @Valid Category category, BindingResult result, Model model){
        if (result.hasErrors()){
            model.addAttribute("formAdd", false);
            return "redirect:/category";
        }
        categoryService.update(category);
        return "redirect:/category";
    }

    @GetMapping("/category/delete/{id}")
    public String delete(@PathVariable("id") int id){
        categoryService.delete(id);
        return "redirect:/category";
    }

}
