package com.example.demo.controller;

import com.example.demo.service.category.CateDBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

    @Autowired
    private CateDBService categoryJDBCService;

    @GetMapping("/multi-languages")
    public String i18n(){
        return "i18n";
    }

    @GetMapping("/test")
    @ResponseBody
    public String test(){
        categoryJDBCService.showAllCategories();
        return "just for test";
    }

}
