package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Bean
    @Profile("kps")
    @Description("Change DataSource in application.properties")
    public DataSource dataSource(){
        DriverManagerDataSource db = new DriverManagerDataSource();
        db.setDriverClassName("org.postgresql.Driver");
        db.setUrl("jdbc:postgresql://localhost:5432/articles_db");
        db.setUsername("postgres");
        db.setPassword("!@#");
        return db;
    }


    @Bean
    @Profile("sr")
    public DataSource sr(){
        DriverManagerDataSource db = new DriverManagerDataSource();
        db.setDriverClassName("org.postgresql.Driver");
        db.setUrl("jdbc:postgresql://localhost:5432/articles_db");
        db.setUsername("postgres");
        db.setPassword("!@#");
        return db;
    }

    //In memory
    @Bean("dataSource")
    @Profile("memory")
    public DataSource inMemory(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
//        builder.addScripts("sql/tables.sql", "sql/data.sql");
        builder.addScript("sql/tables.sql");
        builder.addScript("sql/data.sql");

        return builder.build();
    }
}
