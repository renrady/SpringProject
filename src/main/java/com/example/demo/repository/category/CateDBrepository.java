package com.example.demo.repository.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.sql.*;

@Repository
public class CateDBrepository {

    @Autowired
    private DataSource dataSource;

    public void showAllCategories(){
        try {
            Connection con = dataSource.getConnection();
            Statement statement = con.createStatement();
            String sql = "select * from tb_categories";
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()){
                System.out.println("ID: " + rs.getInt(0) + "Name: " + rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
