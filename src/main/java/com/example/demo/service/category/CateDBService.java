package com.example.demo.service.category;

import com.example.demo.repository.category.CateDBrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CateDBService {

    @Autowired
    private CateDBrepository categoryJDBCRepository;

    public void showAllCategories(){
        categoryJDBCRepository.showAllCategories();
    }
}

