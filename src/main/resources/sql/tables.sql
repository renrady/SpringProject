-- Create table category
Create table tb_categories(
	id int PRIMARY KEY auto_increment,
	category varchar not null
);
-- create table
Create table tb_articles(
	id int Primary key auto_increment,
	title varchar not null,
	description text not null,
	author varchar not null,
	thumbnail varchar not null,
	created_date varchar,
	category_id int not null references tb_categories(id) on delete cascade on update cascade
);
