insert into tb_categories(category)
VALUES ('Spring'),
         ('Web'),
        ('Java'),
        ('Database');

insert into tb_articles(title, description, author, thumbnail, created_date, category_id)
values('Learning', 'Java', 'Rady', '/image/red.jpg', 'Tu Jun 22 10:40:28 ICT 2018', 1);
insert into tb_articles(title, description, author, thumbnail, created_date, category_id)
values('Learning', 'Ajax', 'Rady', '/image/red.jpg', 'Tu Jun 22 10:40:28 ICT 2018', 2);
insert into tb_articles(title, description, author, thumbnail, created_date, category_id)
values('Learning', 'J2EE', 'Rady', '/image/red.jpg', 'Tu Jun 22 10:40:28 ICT 2018', 3);
insert into tb_articles(title, description, author, thumbnail, created_date, category_id)
values('Learning', 'Progresql', 'Rady', '/image/red.jpg', 'Tu Jun 22 10:40:28 ICT 2018', 4);